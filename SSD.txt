----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/04/2021 07:11:28 PM
-- Design Name: 
-- Module Name: SSD - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SSD is
    Port ( Digit0 : in STD_LOGIC_VECTOR (3 downto 0);
           Digit1 : in STD_LOGIC_VECTOR (3 downto 0);
           Digit2 : in STD_LOGIC_VECTOR (3 downto 0);
           Digit3 : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           CAT : out STD_LOGIC_VECTOR (6 downto 0);
           AN : out STD_LOGIC_VECTOR (3 downto 0));
end SSD;

architecture Behavioral of SSD is
signal counter: std_logic_vector(15 downto 0):="0000000000000000";
signal mux1_out: std_logic_vector(3 downto 0):="0000";
signal mux2_out: std_logic_vector(3 downto 0):="0000";
signal mux_in: std_logic_vector(1 downto 0);
signal dcd_in: std_logic_vector(3 downto 0);

begin

process(clk)
begin
mux_in<=counter(15 downto 14);
AN<=mux2_out;
dcd_in<=mux1_out;
if clk'event and clk='1' then
if counter="1111111111111111" then
counter<=x"0000";
else
counter<=counter+1;
end if;
end if;
end process;
process(mux_in)
begin
case mux_in is
when "00" => mux1_out<=Digit0; mux2_out<="1110";
when "01" => mux1_out<=Digit1; mux2_out<="1101";
when "10" => mux1_out<=Digit2; mux2_out<="1011";
when "11" => mux1_out<=Digit3; mux2_out<="0111";
when others => mux1_out<="0000"; mux2_out<="0000";
end case;
end process;
process(dcd_in)
begin
case dcd_in is
  when "0000" => CAT <= "0000001"; -- "0"     
  when "0001" => CAT <= "1001111"; -- "1" 
  when "0010" => CAT <= "0010010"; -- "2" 
  when "0011" => CAT<= "0000110"; -- "3" 
  when "0100" => CAT <= "1001100"; -- "4" 
  when "0101" =>CAT <= "0100100"; -- "5" 
  when "0110" => CAT <= "0100000"; -- "6" 
  when "0111" =>CAT <= "0001111"; -- "7" 
  when "1000" => CAT <= "0000000"; -- "8"     
  when "1001" => CAT <= "0000100"; -- "9" 
  when "1010" =>CAT <= "0000010"; -- a
  when "1011" => CAT <= "1100000"; -- b
  when "1100" =>CAT <= "0110001"; -- C
  when "1101" => CAT <= "1000010"; -- d
  when "1110" =>CAT<= "0110000"; -- E
  when "1111" => CAT <= "0111000"; -- F 
  when others => CAT <="0000001";
end case;
end process;
end Behavioral;
